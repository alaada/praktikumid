package Praktikum6;

import lib.TextIO;

public class Arvamismang {

	public static void main(String[] args) {
		int arvutiarv = suvalineArv(1, 100);
		System.out.println("arvutiArv : " + arvutiarv);

		while (true) {
			System.out.println("Arva ara, mis arv on?");
			int sisestus = TextIO.getlnInt();

			if (sisestus == arvutiarv) {
				System.out.println("Ara arvasid");
				break;
			} else if (sisestus > arvutiarv) {
				System.out.println("Arv on suurem");
			} else {
				System.out.println("Arv on vaiksem");
			}
			
		}
		
	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;

		return (int) (Math.random() * vahemik) + min;
	}

}
