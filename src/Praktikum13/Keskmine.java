package Praktikum13;

import java.util.ArrayList;

public class Keskmine {
	public static void main(String[] args) {
		String kataloogitee = FailiLugeja.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);
		double summa = 0;
		int vigaseidRidu = 0;
	for (String rida : failiSisu) {
		double nr;
		try {
			nr = Double.parseDouble(rida);
		summa += nr;	
		} catch (NumberFormatException e) {
			System.out.println("Viigane numbrite format" + rida);
			vigaseidRidu++;
		}
	}
	double keskmine = summa / (failiSisu.size() + vigaseidRidu);
	System.out.println(keskmine);
	}

}
