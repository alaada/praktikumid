package Praktikum10;

public class Rekursioon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(astenda(2, 3));
int i = 0;
while (true) {
	System.out.println(i + " - " + fibonacciNumber(i));
	i++;
}
	}

	public static int fibonacciNumber(int n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else
			return fibonacciNumber(n - 1) + fibonacciNumber(n - 2);

	}

	public static int astenda(int arv, int aste) {
		// TODO Auto-generated method stub
		if (aste == 0)
			return 1;
		else
			return arv * astenda(arv, aste - 1);
	}

}
