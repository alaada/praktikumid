package Praktikum2;

import lib.TextIO;

public class GruupiSuurus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int inimestearv;

		System.out.println("Sisesta inimeste arvu");
		inimestearv = TextIO.getlnInt();

		int gruupisuurus;

		System.out.println("Sisesta gruupi suurus");
		gruupisuurus = TextIO.getlnInt();

		int vastus = (int) Math.floor(inimestearv / gruupisuurus);

		System.out.println("Saab moodustada " + vastus + " gruppi");

		int j22k = inimestearv % gruupisuurus;
		System.out.println("Jääk on: " + j22k);
	}

}
