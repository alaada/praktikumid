package Praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Yleminek extends Applet {

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		// Kysime kui suur aken on?
		int w = getWidth();
		int h = getHeight();
		double v2rviMuutus = 255. /h; 
		

		// g.setColor(minuV2rv);
		// g.fillRect(100, 200, 200, 100);
		for (int i = 0; i < h; i++) {
			int v2arviKood = (int) (255 - v2rviMuutus * i);
			Color minuV2rv = new Color(i % 255, i % 55, v2arviKood);
			g.setColor(minuV2rv);
			g.drawLine(0, i, w, i);

			// g.setColor(Color.BLUE);
			// g.drawLine(0, i, w, i);
		}

	}
}
