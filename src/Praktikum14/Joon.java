package Praktikum14;

public class Joon {

	Punkt algusPunkt, l6pppunkt;

	public Joon(Punkt p1, Punkt p2) {
		// TODO Auto-generated constructor stub
		algusPunkt = p1;
		l6pppunkt = p2;
	}

	public String toString() {
		return "Joon(" + algusPunkt + " " + l6pppunkt + ")";
	}

	public double pikkus() {
		double a = l6pppunkt.y - algusPunkt.y;
		double b = l6pppunkt.x - algusPunkt.x;
		double h = Math.sqrt(Math.pow(a, 2)+Math.pow(b, 2));
		return h;
	}
	
	
}
