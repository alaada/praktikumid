package Praktikum14;

public class RIng {
	
	Punkt keskpunkt;
	double raadius;
	
	public RIng(Punkt p, double r) {
		// TODO Auto-generated constructor stub
		keskpunkt = p;
		raadius = r;
	}
	
	public double ymberm66t() {
		return 2 * Math.PI * raadius;
	}
	public double pindala() {
		// TODO Auto-generated method stub
		return Math.PI * Math.pow(raadius, 2);
	}
	public String toString() {
		return "Ring, umberm66t " + ymberm66t()
		+ ". pindala on: " +pindala();
	}

}
